FEATURE=LSTM2X_sparse
DB_NAME=dev-clean
EXTENSION=.flac
FEATURE_SIZE=0.01
#METRIC=cosine
METRIC=kl_symmetric

#PATH_AUDIO_DIR=/scratch/librilight/sets/LibriSpeech/$DB_NAME
PATH_AUDIO_DIR=/scratch2/libri-light/unlab_data/LibriSpeech/$DB_NAME

PATH_TO_ABX_ITEMS=/scratch2/libri-light/unlab_data/ABX_data/
#/scratch/librilight/ABX_data

PATH_OUTPUT_CHECKPOINT=./models/1631099782/

# This one is the same as 1631099782, but with the phase removed
PATH_OUTPUT_CHECKPOINT=./models/1639063320/


# This one is with mel spec
PATH_OUTPUT_CHECKPOINT=./models/1639615723/

#PATH_OUTPUT_CHECKPOINT=./models/1628616366/

#for DB_NAME in dev-clean dev-other test-clean test-other; do
        OUTPUT_DIR=evals/eval_${FEATURE}_${DB_NAME}_${METRIC}/
        mkdir -p $OUTPUT_DIR
        echo "computing ABX measure for $DB_NAME. Putting results in $OUTPUT_DIR."
        # default --max_size_group 10
        CUDA_VISIBLE_DEVICES=1 python3 eval_ABX.py $PATH_AUDIO_DIR  $PATH_TO_ABX_ITEMS/$DB_NAME.item --file_extension $EXTENSION --out $OUTPUT_DIR --feature_size $FEATURE_SIZE --distance_mode $METRIC --path_checkpoint $PATH_OUTPUT_CHECKPOINT --cuda #--max_size_group 10 --max_x_across 5 
#done

