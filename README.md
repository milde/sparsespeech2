# Sparsespeech-2

This is the second and rewritten PyTorch version of Sparsespeech. For the original Tensorflow 1.x implementation see [https://gitlab.com/milde/sparsespeech](this repository).

Other than using PyTorch, the focus in this implementation is on end-to-end modeling of posterior-like speech representations.
This also means that there is no dependency on computing the features with Kaldi anymore.  

Unfortunatly, the current results are not yet on par with the Tensorflow reference implementation.

# References

    @inproceedings{milde19_interspeech,
      author={Benjamin Milde and Chris Biemann},
      title={{SparseSpeech: Unsupervised Acoustic Unit Discovery with Memory-Augmented Sequence Autoencoders}},
      year=2019,
      booktitle={Proc. Interspeech 2019},
      pages={256--260},
      address={Graz, Austria},
      doi={10.21437/Interspeech.2019-2938}
    }

    @inproceedings{milde20_interspeech,
      author={Benjamin Milde and Chris Biemann},
      title={{Improving Unsupervised Sparsespeech Acoustic Models with Categorical Reparameterization}},
      year=2020,
      booktitle={Proc. Interspeech 2020},
      pages={2747--2751},
      address={Virtual Shanghai, China},
      doi={10.21437/Interspeech.2020-2629}
    }
