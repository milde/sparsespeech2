
# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.
import argparse
import json
import os
import numpy as np
import torch
import time
from copy import deepcopy
import random
import psutil
import sys
import visdom

import sparsespeech2
import utils
import feature_loader as fl

from sklearn.cluster import KMeans

#import cpc.criterion as cr
#import cpc.model as model
#import cpc.utils.misc as utils

from dataset import AudioBatchData, findAllSeqs, filterSeqs, parseSeqLabels

class DefaultGumbelConfig:
    temperature_start = 2.0
    temperature_current = temperature_start
    temperature_stop = 0.05
    temperature_scale = 0.999965
    temperature_inference = 0.2
    kmeans_init_batches = 128*2
    gumbel_epoch_start = 1
    init_epochs = 100

def diversity_kl_dense(q,n):
    return -1.0*torch.mean(torch.maximum(q,torch.zeros(n).cuda())-q*q*n+torch.log(1.+torch.exp(-1.*torch.abs(q)))) 

#def getCriterion(args, downsampling, nSpeakers, nPhones):
#    dimFeatures = args.hiddenGar if not args.onEncoder else args.hiddenEncoder
#    if not args.supervised:
#        if args.cpc_mode == 'none':
#            spCriterion = cr.NoneCriterion()
#        else:
#            sizeInputSeq = (args.sizeWindow // downsampling)
#            spCriterion = cr.CPCUnsupersivedCriterion(args.nPredicts,
#                                                       args.hiddenGar,
#                                                       args.hiddenEncoder,
#                                                       args.negativeSamplingExt,
#                                                       mode=args.cpc_mode,
#                                                       rnnMode=args.rnnMode,
#                                                       dropout=args.dropout,
#                                                       nSpeakers=nSpeakers,
#                                                       speakerEmbedding=args.speakerEmbedding,
#                                                       sizeInputSeq=sizeInputSeq)
#    elif args.pathPhone is not None:
#        if not args.CTC:
#            spCriterion = cr.PhoneCriterion(dimFeatures,
#                                             nPhones, args.onEncoder,
#                                             nLayers=args.nLevelsPhone)
#        else:
#            spCriterion = cr.CTCPhoneCriterion(dimFeatures,
#                                                nPhones, args.onEncoder)
#    else:
#        spCriterion = cr.SpeakerCriterion(dimFeatures, nSpeakers)
#    return spCriterion

def getCriterion(args, downsampling, criterion_type='FFT'):
    dimFeatures = args.hiddenGar #if not args.onEncoder else args.hiddenEncoder

    if criterion_type=='FFT':
        spCriterion = sparsespeech2.FFTCriterion(dimEncoder=dimFeatures)
        print('Created FFT Criterion with dim encoder:', dimFeatures) 
    elif criterion_type=='Mel':
        spCriterion = sparsespeech2.MelCriterion(dimEncoder=dimFeatures, n_mels=args.criterion_n_mels)
        print('Created MelCriterion with n_mels:', args.criterion_n_mels, 'dim encoder:', dimFeatures)
    return spCriterion

def loadCriterion(pathCheckpoint, downsampling):
    _, _, locArgs = fl.getCheckpointData(os.path.dirname(pathCheckpoint))
    criterion = getCriterion(locArgs, downsampling)

    state_dict = torch.load(pathCheckpoint, 'cpu')

    criterion.load_state_dict(state_dict["spCriterion"])
    return criterion


def trainStep(dataLoader,
              spModel,
              spCriterion,
              clusterCriterion,
              optimizer,
              scheduler,
              loggingStep,
              epoch,
              gumbel_config,
              use_gumbel=False,
              initial_gumbel_init=False,
              debugOutputWav='output_wav_test/',
              vis=None,
              total_train_steps=0,
              use_kl=True
              ):


    spModel.train()
    spCriterion.train()
    clusterCriterion.train()

    start_time = time.perf_counter()
    n_examples = 0
    logs, lastlogs = {}, None
    iter = 0

    uniform_dist = None
    collected_batches = 0
    cluster_init_completed=False

    k_means_batches = []
    k_means_cFeature_raws = []

    for step, fulldata in enumerate(dataLoader):

        #print('step:', step)

        batchData, label = fulldata
        n_examples += batchData.size(0)

        if not (initial_gumbel_init and cluster_init_completed):
            batchData_cuda = batchData.cuda(non_blocking=True)
            label_cuda = label.cuda(non_blocking=True)

            raw_encodedData, encodedData, cFeature, cOutput, decodedData, query, cFeature_cc_seq_masked, cFeature_raw, logits = \
                spModel(batchData_cuda, label, temperature=gumbel_config.temperature_current, use_gumbel_softmax=use_gumbel)

        if initial_gumbel_init and not cluster_init_completed:
            spModel.eval()
            spCriterion.eval()
            clusterCriterion.eval()

            # here we run K-means to get init labels for the memory bank
            # we need a couple of batches for a good initialization, but since its just
            # a starting point we don't need to do it over the whole dataset

            if collected_batches < gumbel_config.kmeans_init_batches:
                k_means_batches.append(batchData)
                k_means_cFeature_raws.append(np.copy(np.vstack(cFeature_raw.detach().cpu().numpy())))
                collected_batches += 1
            else:  #start clustering (we collected the cFeature_raw vectors in k_means_cFeature_raws)
                print('Init batches collected, starting clustering')

                cFeature_stacked = np.vstack(k_means_cFeature_raws)

                print('Clustering cFeature_stacked numpy array with shape:', cFeature_stacked.shape)

                kmeans = KMeans(n_clusters=spModel.module.memory_n, init='k-means++', n_init=12).fit(cFeature_stacked)

                cluster_centers = np.asarray(kmeans.cluster_centers_)

                print('assign cluster_centers to memory module')
                spModel.module.gumbelmem.value_memory.data = torch.as_tensor(cluster_centers, dtype=torch.float32).cuda()

                predictions = np.asarray(kmeans.labels_)

                print('First 100 predictions:')
                print(predictions[:100])

                cluster_init_completed = True

            continue

        elif initial_gumbel_init and cluster_init_completed:

            spModel.train()
            spCriterion.train()
            clusterCriterion.train()

            # run the model through the kmeans batches and let it predict the labels

            for epoch in range(gumbel_config.init_epochs):
                pos = 0
                print('Gumnel init epoch:', epoch)
                for batchData in k_means_batches:

                    batchData_cuda = batchData.cuda(non_blocking=True)

                    raw_encodedData, encodedData, cFeature, cOutput, decodedData, query, cFeature_cc_seq_masked, cFeature_raw, logits = \
                        spModel(batchData_cuda, label, temperature=gumbel_config.temperature_current,
                                use_gumbel_softmax=use_gumbel)

                    len_batch = query.shape[0] * query.shape[1]

                    #print('len_batch:', len_batch)

                    pred = predictions[pos:pos+len_batch] #.reshape(batchData.shape[0],-1)
                    # learn to predict to predict cluster labels

                    #print('query shape', query.shape)
                    #print('pred shape', pred.shape)

                    query_reshaped = query.reshape(-1, query.shape[2])

                    #print('query reshaped', query_reshaped.shape)

                    allLosses = clusterCriterion(query_reshaped, torch.as_tensor(pred, dtype=torch.int64).cuda())
                    totLoss = allLosses.sum()

                    print('totLoss:', totLoss.detach().cpu().numpy())

                    totLoss.backward()

                    optimizer.step()
                    optimizer.zero_grad()

                    pos += len_batch

            print('Gumbel init completed!')

            initial_gumbel_init=False
            continue
        else:
            allLosses = spCriterion(batchData_cuda, encodedData, cFeature, cOutput, decodedData)
            totLoss = allLosses.sum()

            if use_gumbel and use_kl:
                query_seq_mean = torch.mean(query, dim=-2)

                #diversity_kl_dense(q,n)
                kl_term = diversity_kl_dense(query_seq_mean, spModel.module.memory_n)

                totLoss += kl_term

# old version of kl
 #           if use_gumbel and use_kl:
 #               if uniform_dist is None or query.size() != uniform_dist.size():
 #                   uniform_dist = torch.ones_like(query)
 #                   # print("Created uniform dist with size: ", uniform_dist.size())
 #                   uniform_dist /= spModel.module.memory_n
#
                #note that kl_div expects log probabilities for the first argument
#                kl_term = torch.nn.functional.kl_div(torch.nn.functional.log_softmax(logits, dim=2), uniform_dist,
#                                                     reduction='mean')
#                totLoss += 0.3 * kl_term

            totLoss.backward()

            # Show grads ?
            optimizer.step()
            optimizer.zero_grad()

        if "locLoss_train" not in logs:
            logs["locLoss_train"] = 0.0 #np.zeros(allLosses.size(1))
            #logs["locAcc_train"] = np.zeros(allLosses.size(1))

        iter += 1
        step_loss = totLoss.detach().cpu().numpy()
        logs["locLoss_train"] += step_loss #(allLosses.mean(dim=0)).detach().cpu().numpy()
        #logs["locAcc_train"] += (allAcc.mean(dim=0)).cpu().numpy()

        #print('step loss:', step_loss)

        total_train_steps += 1

        if (step + 1) % loggingStep == 0:
            print("step", step, 'use gumbel:', use_gumbel , 'gumbel temperature:', gumbel_config.temperature_current)

            wav_input_file = debugOutputWav + 'orig_epoch%.2i_step%.7i.wav' % (epoch,step)
            wav_output_file = debugOutputWav + 'decoded_epoch%.2i_step%.7i.wav' % (epoch,step)
            with open(wav_input_file, 'wb') as wav_input, \
                open(wav_output_file, 'wb') as wav_output:

                example_orig = batchData_cuda.squeeze().cpu().numpy()[0]
                example_decoded = decodedData.detach().squeeze().cpu().numpy()[0]

                print('example_orig:', example_orig)
                print('example_decoded:', example_decoded)

                wav_input.write(utils.float32_wav_file(example_orig, 16000))
                wav_output.write(utils.float32_wav_file(example_decoded, 16000))

                print('Wrote example wav to',wav_input_file,'and',wav_output_file)

            new_time = time.perf_counter()
            elapsed = new_time - start_time
            print(f"Update {step + 1}")
            print(f"elapsed: {elapsed:.1f} s")
            print(f"{1000. * elapsed / loggingStep:.1f} ms per batch, {1000. * elapsed / n_examples:.1f} ms / example")
            locLogs = utils.update_logs(logs, loggingStep, lastlogs)
            lastlogs = deepcopy(logs)
            print("Training loss", locLogs)

            if use_gumbel:
                print("Gumbel softmax is activated")
                if use_kl:
                    print("KL Term:", kl_term.detach().cpu().numpy())
            else:
                print("Gumbel softmax is off")

            print("Epoch:", epoch)
            #utils.show_logs("Training loss", locLogs)
            start_time, n_examples = new_time, 0

            if vis is not None:
                if query is not None:
                    vis.heatmap(query.detach().cpu()[0].T, win='query', env=str(spModel.module.model_id),
                            opts=dict(title='query'))
                vis.heatmap(raw_encodedData.detach().cpu()[0].T, win='raw_encodedData', env=str(spModel.module.model_id),
                            opts=dict(title='raw_encodedData'))
                vis.heatmap(encodedData.detach().cpu()[0].T, win='encodedData', env=str(spModel.module.model_id), opts=dict(title='encodedData'))
                vis.heatmap(cFeature.detach().cpu()[0].T, win='cFeature', env=str(spModel.module.model_id), opts=dict(title='cFeature'))
                vis.heatmap(cOutput.detach().cpu()[0].T, win='cOutput', env=str(spModel.module.model_id), opts=dict(title='cOutput'))

                vis.heatmap(cFeature_cc_seq_masked.detach().cpu()[0].T, win='cFeature_cc_seq_masked', env=str(spModel.module.model_id),
                            opts=dict(title='cFeature_cc_seq_masked'))

                vis.heatmap(decodedData.detach().cpu()[0].T, win='decodedData', env=str(spModel.module.model_id),
                            opts=dict(title='decodedData'))

                vis.heatmap(batchData.detach().cpu()[0].T, win='batchData', env=str(spModel.module.model_id),
                            opts=dict(title='batchData'))

                vis.heatmap(spModel.module.gumbelmem.value_memory.detach().cpu(), win='memory_heatmap',
                            env=str(spModel.module.model_id), opts=dict(title='memory_heatmap'))

                vis.line(Y=np.array([gumbel_config.temperature_current]), X=np.array([total_train_steps]), win='gumbelt', env=str(spModel.module.model_id),
                         update='append')

                vis.line(Y=np.array([locLogs['locLoss_train']]), X=np.array([total_train_steps]), win='locLoss_train', env=str(spModel.module.model_id),
                         update='append')

        if use_gumbel and not initial_gumbel_init:
            if gumbel_config.temperature_current > gumbel_config.temperature_stop:
                gumbel_config.temperature_current *= gumbel_config.temperature_scale

    if scheduler is not None:
        scheduler.step()

    logs = utils.update_logs(logs, iter)
    logs["iter"] = iter
    print('epoch end logs:', logs)
    #utils.show_logs("Average training loss on epoch", logs)
    return logs, total_train_steps


def valStep(dataLoader,
            spModel,
            spCriterion,
            gumbel_config,
            use_gumbel):

    spCriterion.eval()
    spModel.eval()
    logs = {}
    spCriterion.eval()
    spModel.eval()
    iter = 0

    for step, fulldata in enumerate(dataLoader):

        batchData, label = fulldata

        batchData = batchData.cuda(non_blocking=True)
        label = label.cuda(non_blocking=True)

        with torch.no_grad():
            raw_encodedData, encodedData, cFeature, cOutput, decodedData, query, cFeature_cc_seq_masked, cFeature_raw, \
                logits = spModel(batchData, label, temperature=gumbel_config.temperature_inference,
                                 use_gumbel_softmax=use_gumbel)
            allLosses = spCriterion(batchData, encodedData, cFeature, cOutput, decodedData)

            #c_feature, encoded_data, label = spModel(batchData, label)
            #allLosses, allAcc = spCriterion(c_feature, encoded_data, label)
            #allLosses = spCriterion(c_feature, encoded_data, label)

        if "locLoss_val" not in logs:
            logs["locLoss_val"] = 0 #np.zeros(allLosses.size(1))
 #           logs["locAcc_val"] = np.zeros(allLosses.size(1))

        iter += 1
        logs["locLoss_val"] += allLosses.detach().cpu().numpy()
 #       logs["locLoss_val"] += allLosses.mean(dim=0).cpu().numpy()
 #       logs["locAcc_val"] += allAcc.mean(dim=0).cpu().numpy()

    logs = utils.update_logs(logs, iter)
    logs["iter"] = iter
    #utils.show_logs("Validation loss:", logs)
    print("Validation loss:", logs)

    return logs


def run(trainDataset,
        valDataset,
        batchSize,
        samplingMode,
        spModel,
        spCriterion,
        clusterCriterion,
        nEpoch,
        pathCheckpoint,
        optimizer,
        scheduler,
        gumbel_config,
        logs,
        vis):

    print(f"Running {nEpoch} epochs")
    startEpoch = len(logs["epoch"])
    print(f"start epoch is: {startEpoch}")
    bestLoss = 10000000
    bestStateDict = None
    start_time = time.time()

    print('pathCheckpoint:', pathCheckpoint)

    debugOutputWav = pathCheckpoint.replace('checkpoint','') + 'debugOutputWav/'

    utils.ensure_dir(debugOutputWav)

    total_train_steps = 0

    for epoch in range(startEpoch, nEpoch):

        use_gumbel = False if epoch < gumbel_config.gumbel_epoch_start else True
        print(f"Starting epoch {epoch}, use gumbel:", use_gumbel)
        utils.cpu_stats()

        trainLoader = trainDataset.getDataLoader(batchSize, samplingMode,
                                                 True, numWorkers=0)

        valLoader = valDataset.getDataLoader(batchSize, 'sequential', False,
                                             numWorkers=0)

        print("Training dataset %d batches, Validation dataset %d batches, batch size %d" %
              (len(trainLoader), len(valLoader), batchSize))

        locLogsTrain, total_train_steps = trainStep(trainLoader, spModel, spCriterion, clusterCriterion,
                                 optimizer, scheduler, logs["logging_step"], epoch, gumbel_config,
                                 debugOutputWav=debugOutputWav, vis=vis, use_gumbel=use_gumbel,
                                                    initial_gumbel_init= (epoch==gumbel_config.gumbel_epoch_start),
                                                    total_train_steps=total_train_steps)

        locLogsVal = valStep(valLoader, spModel, spCriterion, gumbel_config, use_gumbel=use_gumbel)

        print(f'Ran {epoch + 1} epochs '
              f'in {time.time() - start_time:.2f} seconds')

        print("Total train steps:", total_train_steps)

        torch.cuda.empty_cache()

        currentLoss = float(locLogsVal["locLoss_val"])

        print('Validation loss is:', currentLoss)

        if currentLoss < bestLoss:
            print(currentLoss,'is better than', bestLoss)
            print('Updating bestStateDict.')

            bestStateDict = fl.get_module(spModel).state_dict()

        for key, value in dict(locLogsTrain, **locLogsVal).items():
            if key not in logs:
                logs[key] = [None for x in range(epoch)]
            if isinstance(value, np.ndarray):
                value = value.tolist()
            logs[key].append(value)

        logs["epoch"].append(epoch)

        if pathCheckpoint is not None:
            print('Saving model to',f"{pathCheckpoint}_{epoch}.pt")
  #              and (epoch % logs["saveStep"] == 0 or epoch == nEpoch-1):

            modelStateDict = fl.get_module(spModel).state_dict()
            criterionStateDict = fl.get_module(spCriterion).state_dict()

            fl.save_checkpoint(modelStateDict, criterionStateDict,
                               optimizer.state_dict(), bestStateDict,
                               f"{pathCheckpoint}_{epoch}.pt")
            utils.save_logs(logs, pathCheckpoint + "_logs.json")
        else:
            print('pathCheckpoint is None, not saving Model')

def main(args):
    args = parseArgs(args)

    utils.set_seed(args.random_seed)
    logs = {"epoch": [], "iter": [], "saveStep": args.save_step}
    loadOptimizer = False
    if args.pathCheckpoint is not None and not args.restart:
        cdata = fl.getCheckpointData(args.pathCheckpoint)
        if cdata is not None:
            data, logs, locArgs = cdata
            print(f"Checkpoint detected at {data}")
            fl.loadArgs(args, locArgs,
                        forbiddenAttr={"nGPU", "pathCheckpoint",
                                       "debug", "restart", "world_size",
                                       "n_nodes", "node_id", "n_gpu_per_node",
                                       "max_size_loaded"})
            args.load, loadOptimizer = [data], True
            args.loadCriterion = True

    if args.pathCheckpoint is None:
        model_id = str(int(time.time()))
        model_dir = './models/' + model_id + '/'

        args.pathCheckpoint = model_dir
        print('Training with new model dir:', args.pathCheckpoint)


    logs["logging_step"] = args.logging_step

    config_json_s = json.dumps(vars(args), indent=4, sort_keys=True) 
    print(f'CONFIG:\n{config_json_s}')

    utils.ensure_dir(model_dir)

    with open(model_dir + 'config.json', 'w') as config_output_file:
        config_output_file.write(config_json_s + '\n')

    print('-' * 50)

    seqNames, speakers = findAllSeqs(args.pathDB,
                                     extension=args.file_extension,
                                     loadCache=not args.ignore_cache)

    print(f'Found files: {len(seqNames)} seqs, {len(speakers)} speakers')
    # Datasets
    if args.pathTrain is not None:
        seqTrain = filterSeqs(args.pathTrain, seqNames)
    else:
        seqTrain = seqNames

    if args.pathVal is None:
        random.shuffle(seqTrain)
        sizeTrain = int(0.99 * len(seqTrain))
        seqTrain, seqVal = seqTrain[:sizeTrain], seqTrain[sizeTrain:]
        print(f'Found files: {len(seqTrain)} train, {len(seqVal)} val')
    else:
        seqVal = filterSeqs(args.pathVal, seqNames)

    if args.debug:
        seqTrain = seqTrain[-40:]
        seqVal = seqVal[-4:]

    phoneLabels, nPhones = None, None
    if args.supervised and args.pathPhone is not None:
        print("Loading the phone labels at " + args.pathPhone)
        phoneLabels, nPhones = parseSeqLabels(args.pathPhone)
        print(f"{nPhones} phones found")

    print("")
    print(f'Loading audio data at {args.pathDB}')
    print("Loading the training dataset")
    trainDataset = AudioBatchData(args.pathDB,
                                  args.sizeWindow,
                                  seqTrain,
                                  phoneLabels,
                                  len(speakers),
                                  nProcessLoader=args.n_process_loader,
                                  MAX_SIZE_LOADED=args.max_size_loaded)
    print("Training dataset loaded")
    print("")

    print("Loading the validation dataset")
    valDataset = AudioBatchData(args.pathDB,
                                args.sizeWindow,
                                seqVal,
                                phoneLabels,
                                len(speakers),
                                nProcessLoader=args.n_process_loader)
    print("Validation dataset loaded")
    print("")

    if args.load is not None:
        print("model load not yet implemented")
        sys.exit(-1)
        spModel, args.hiddenGar, args.hiddenEncoder = fl.loadModel(args.load)

    else:
        # hiddenEncoder = Hidden dimension of the encoder network.
        # hiddenGar = Hidden dimension of the auto-regressive network

        spModel = sparsespeech2.SparseSpeechModel(sizeWindow=args.sizeWindow, EncoderDecoderType=args.encoderDecoder,
                                                  hiddenDim=args.hiddenEncoder, rawEncodedDim=args.hiddenGar,
                                                  nLevels=args.nLevels, maskingProb=args.maskingProb, abspos=args.abspos,
                                                  num_heads_transformer=args.nHeads, model_id=model_id)

    batchSize = args.nGPU * args.batchSizeGPU
    spModel.supervised = args.supervised

    clusterCriterion = torch.nn.CrossEntropyLoss()

    # Training criterion
    if args.load is not None and args.loadCriterion:
        spCriterion = loadCriterion(args.load[0], spModel.raw_encoder.DOWNSAMPLING)
    else:
        print("Get criterion...")
        spCriterion = getCriterion(args, spModel.raw_encoder.DOWNSAMPLING)

    if loadOptimizer:
        print("Loading optimizer " + args.load[0])
        state_dict = torch.load(args.load[0], 'cpu')
        spCriterion.load_state_dict(state_dict["spCriterion"])

    spCriterion.cuda()
    spModel.cuda()
    clusterCriterion.cuda()

    # Optimizer
    g_params = list(spCriterion.parameters()) + list(spModel.parameters())

    lr = args.learningRate

    if args.optimizer == 'Adam':
        optimizer = torch.optim.Adam(g_params, lr=lr,
                                 betas=(args.beta1, args.beta2),
                                 eps=args.epsilon)
    elif args.optimizer == 'AdamW':
        optimizer = torch.optim.AdamW(g_params, lr=lr,
                                 betas=(args.beta1, args.beta2),
                                 eps=args.epsilon)
    else:
        print('Optimizer', args.optimizer, 'not supported.')
        sys.exit(-45)

    if loadOptimizer:
        print("Loading optimizer " + args.load[0])
        state_dict = torch.load(args.load[0], 'cpu')
        if "optimizer" in state_dict:
            optimizer.load_state_dict(state_dict["optimizer"])

    # Checkpoint
    if args.pathCheckpoint is not None:
        if not os.path.isdir(args.pathCheckpoint):
            os.mkdir(args.pathCheckpoint)
        args.pathCheckpoint = os.path.join(args.pathCheckpoint, "checkpoint")

    scheduler = None
    if args.schedulerStep > 0:
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer,
                                                    args.schedulerStep,
                                                    gamma=0.5)
    if args.schedulerRamp is not None:
        n_epoch = args.schedulerRamp
        print(f"Ramp activated. n_e = {n_epoch}")
        scheduler_ramp = torch.optim.lr_scheduler.LambdaLR(optimizer,
                                                           lr_lambda=lambda epoch: utils.ramp_scheduling_function(
                                                               n_epoch, epoch),
                                                           last_epoch=-1)
        if scheduler is None:
            scheduler = scheduler_ramp
        else:
            scheduler = utils.SchedulerCombiner([scheduler_ramp, scheduler],
                                                [0, args.schedulerRamp])
    if scheduler is not None:
        for i in range(len(logs["epoch"])):
            scheduler.step()

    spModel = torch.nn.DataParallel(spModel, device_ids=range(args.nGPU)).cuda()
    spCriterion = torch.nn.DataParallel(spCriterion, device_ids=range(args.nGPU)).cuda()
    clusterCriterion = torch.nn.DataParallel(clusterCriterion, device_ids=range(args.nGPU)).cuda()

    vis = visdom.Visdom()

    gumbel_config = DefaultGumbelConfig()

    run(trainDataset,
        valDataset,
        batchSize,
        args.samplingType,
        spModel,
        spCriterion,
        clusterCriterion,
        args.nEpoch,
        args.pathCheckpoint,
        optimizer,
        scheduler,
        gumbel_config,
        logs,
        vis=vis)


def get_default_cpc_config():
    parser = set_default_cpc_config(argparse.ArgumentParser())
    return parser.parse_args([])


def set_default_cpc_config(parser):
    # Run parameters

    group = parser.add_argument_group('Architecture configuration',
                                      description="The arguments defining the "
                                      "model's architecture.")
    group.add_argument('--hiddenEncoder', type=int, default=256,
                       help='Hidden dimension of the encoder network.')
    group.add_argument('--hiddenGar', type=int, default=512,
                       help='Hidden dimension of the auto-regressive network')
    group.add_argument('--nPredicts', type=int, default=12,
                       help='Number of steps to predict.')
    group.add_argument('--negativeSamplingExt', type=int, default=128,
                       help='Number of negative samples to take.')
    group.add_argument('--learningRate', type=float, default=1e-4)
    group.add_argument('--schedulerStep', type=int, default=-1,
                       help='Step of the learning rate scheduler: at each '
                       'step the learning rate is divided by 2. Default: '
                       'no scheduler.')
    group.add_argument('--schedulerRamp', type=int, default=None,
                       help='Enable a warm up phase for the learning rate: '
                       'adds a linear ramp of the given size.')
    group.add_argument('--beta1', type=float, default=0.9,
                       help='Value of beta1 for the Adam optimizer')
    group.add_argument('--beta2', type=float, default=0.999,
                       help='Value of beta2 for the Adam optimizer')
    group.add_argument('--epsilon', type=float, default=1e-08,
                       help='Value of epsilon for the Adam optimizer')
    group.add_argument('--sizeWindow', type=int, default=20480,
                       help='Number of frames to consider at each batch.')
    group.add_argument('--nEpoch', type=int, default=20,
                       help='Number of epoch to run')
    group.add_argument('--samplingType', type=str, default='samespeaker',
                       choices=['samespeaker', 'uniform',
                                'samesequence', 'sequential'],
                       help='How to sample the negative examples in the '
                       'CPC loss.')
    group.add_argument('--criterion_n_mels', type=int, default=40,
                       help='Number of mels for the loss function,'
                       'if MelCriterion is used.')

    group.add_argument('--nLevelsPhone', type=int, default=1,
                       help='(Supervised mode only). Number of layers in '
                       'the phone classification network.')
#    group.add_argument('--cpc_mode', type=str, default=None,
#                       choices=['reverse', 'none'],
#                       help='Some variations on CPC.')
    group.add_argument('--encoder_type', type=str,
                       choices=['cpc', 'mfcc', 'lfb'],
                       default='cpc',
                       help='Replace the encoder network by mfcc features '
                       'or learned filter banks')
    group.add_argument('--normMode', type=str, default='layerNorm',
                       choices=['instanceNorm', 'ID', 'layerNorm',
                                'batchNorm'],
                       help="Type of normalization to use in the encoder "
                       "network (default is layerNorm).")
    group.add_argument('--onEncoder', action='store_true',
                       help="(Supervised mode only) Perform the "
                       "classification on the encoder's output.")
    group.add_argument('--random_seed', type=int, default=None,
                       help="Set a specific random seed.")
    group.add_argument('--encoderDecoder', default='LSTM',
                       choices=['LSTM', 'transformer', 'transformerWT'],
                       help="Architecture to use for the encoder/decoder"
                       "network (default is lstm).")
    group.add_argument('--nLevels', type=int, default=2,
                       help='Number of layers in the encoder/decoder network.')
    group.add_argument('--nHeads', type=int, default=16,
                       help='Number of attention heads to use in a transformer network.')
    group.add_argument('--dropout', action='store_true',
                       help="Add a dropout layer at the output of the "
                       "prediction network.")
    group.add_argument('--abspos', action='store_true',
                       help='If the prediction network is a transformer, '
                       'active to use absolute coordinates.')
    group.add_argument('--optimizer', type=str, default='Adam',
                       choices=['Adam', 'AdamW'],
                       help='Optimizer being used to train the network.')
    group.add_argument('--maskingProb', type=float, default=0.2,
                       help='Masking probability in the intermediate representations')
    group.add_argument('--numUnits', type=float, default=40,
                        help='Num units in the memory network (for gumbel softmax and kmeans initialization)')
    group.add_argument('--inputEndcodingProj', type=float, default=100,
                        help='Size of the input encoding projection')

    return parser

def parseArgs(argv):
    # Run parameters
    parser = argparse.ArgumentParser(description='Trainer')

    # Default arguments:
    parser = set_default_cpc_config(parser)

    group_db = parser.add_argument_group('Dataset')
    group_db.add_argument('--pathDB', type=str, default="/scratch/librilight/small",
                          help='Path to the directory containing the '
                          'data.')
    group_db.add_argument('--file_extension', type=str, default=".flac",
                          help="Extension of the audio files in the dataset.")
    group_db.add_argument('--pathTrain', type=str, default=None,
                          help='Path to a .txt file containing the list of the '
                          'training sequences.')
    group_db.add_argument('--pathVal', type=str, default=None,
                          help='Path to a .txt file containing the list of the '
                          'validation sequences.')
    group_db.add_argument('--n_process_loader', type=int, default=8,
                          help='Number of processes to call to load the '
                          'dataset')
    group_db.add_argument('--ignore_cache', action='store_true',
                          help='Activate if the dataset has been modified '
                          'since the last training session.')
    group_db.add_argument('--max_size_loaded', type=int, default=4000000000,
                          help='Maximal amount of data (in byte) a dataset '
                          'can hold in memory at any given time')
    group_supervised = parser.add_argument_group(
        'Supervised mode (depreciated)')
    group_supervised.add_argument('--supervised', action='store_true',
                                  help='(Depreciated) Disable the CPC loss and activate '
                                  'the supervised mode. By default, the supervised '
                                  'training method is the speaker classification.')
    group_supervised.add_argument('--pathPhone', type=str, default=None,
                                  help='(Supervised mode only) Path to a .txt '
                                  'containing the phone labels of the dataset. If given '
                                  'and --supervised, will train the model using a '
                                  'phone classification task.')
    group_supervised.add_argument('--CTC', action='store_true')

    group_save = parser.add_argument_group('Save')
    group_save.add_argument('--pathCheckpoint', type=str, default=None,
                            help="Path of the output directory.")
    group_save.add_argument('--logging_step', type=int, default=1000)
    group_save.add_argument('--save_step', type=int, default=1,
                            help="Frequency (in epochs) at which a checkpoint "
                            "should be saved")

    group_load = parser.add_argument_group('Load')
    group_load.add_argument('--load', type=str, default=None, nargs='*',
                            help="Load an exsiting checkpoint. Should give a path "
                            "to a .pt file. The directory containing the file to "
                            "load should also have a 'checkpoint.logs' and a "
                            "'checkpoint.args'")
    group_load.add_argument('--loadCriterion', action='store_true',
                            help="If --load is activated, load the state of the "
                            "training criterion as well as the state of the "
                            "feature network (encoder + AR)")
    group_load.add_argument('--restart', action='store_true',
                            help="If any checkpoint is found, ignore it and "
                            "restart the training from scratch.")

    group_gpu = parser.add_argument_group('GPUs')
    group_gpu.add_argument('--nGPU', type=int, default=-1,
                           help="Number of GPU to use (default: use all "
                           "available GPUs)")
    group_gpu.add_argument('--batchSizeGPU', type=int, default=8,
                           help='Number of batches per GPU.')
    parser.add_argument('--debug', action='store_true',
                        help="Load only a very small amount of files for "
                        "debugging purposes.")
    args = parser.parse_args(argv)

    if args.pathDB is None and (args.pathCheckpoint is None or args.restart):
        parser.print_help()
        print("Either provides an input dataset or a checkpoint to load")
        sys.exit()

    if args.pathCheckpoint is not None:
        args.pathCheckpoint = os.path.abspath(args.pathCheckpoint)

    if args.load is not None:
        args.load = [os.path.abspath(x) for x in args.load]

    # set it up if needed, so that it is dumped along with other args
    if args.random_seed is None:
        args.random_seed = random.randint(0, 2**31)

    if args.nGPU < 0:
        args.nGPU = torch.cuda.device_count()
    assert args.nGPU <= torch.cuda.device_count(),\
        f"number of GPU asked: {args.nGPU}," \
        f"number GPU detected: {torch.cuda.device_count()}"
    print(f"Let's use {args.nGPU} GPUs!")

    return args


if __name__ == "__main__":
    torch.multiprocessing.set_start_method('spawn')
    args = sys.argv[1:]
    main(args)
