# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.
import torch
import torchaudio
import os
import json
import time
import argparse
from cpc_default_config import get_default_cpc_config
from dataset import parseSeqLabels

import sparsespeech2
from sparsespeech2 import SparseSpeechModel


#class FeatureModule(torch.nn.Module):
#    r"""
#    A simpler interface to handle SP models. Useful for a smooth workflow when
#    working with SP trained features.
#    """
#
#    def __init__(self, featureMaker, get_encoded, collapse=False):
#        super(FeatureModule, self).__init__()
#        self.get_encoded = get_encoded
#        self.featureMaker = featureMaker
#        self.collapse = collapse
#
#    def getDownsamplingFactor(self):
#        return self.featureMaker.gEncoder.DOWNSAMPLING
#
#    def forward(self, data):
#
#        batchAudio, label = data
#        cFeature, encoded, _ = self.featureMaker(batchAudio.cuda(), label)
#        if self.get_encoded:
#            cFeature = encoded
#        if self.collapse:
#            cFeature = cFeature.contiguous().view(-1, cFeature.size(2))
#        return cFeature

def load_sp_featuremaker(path_checkpoint):
    model, config = loadModel(path_checkpoint, loadStateDict=True)
    model.eval()
    output = FeatureModule(model, get_encoded=False)
    output.config = config
    return output

class FeatureModule(torch.nn.Module):
    r"""
    A simpler interface to handle SP models. Useful for a smooth workflow when
    working with SP trained features.
    """

    def __init__(self, featureMaker, get_encoded=False,
                 seq_norm=False):
        super(FeatureModule, self).__init__()
        self.get_encoded = get_encoded
        self.model = featureMaker
        #self.model.set_masking_prob(0.)
        self.seq_norm = seq_norm
        self.config = None

    def forward(self, batch_data):

        self.model.eval()
        # Input Size : BatchSize x 1 x SeqSize
        # Feature size: BatchSize x SeqSize x ChannelSize
        if self.is_cuda:
            batch_data = batch_data.cuda()

        #   return encodedData, cFeature, cOutput, decodedData
#        encodedData, cFeature, cOutput, decodedData = self.model(batch_data, None)
        
        with torch.no_grad():
            raw_encodedData, encodedData, cFeature, cOutput, decodedData, query, cFeature_cc_seq_masked, cFeature_raw, logits = self.model(batch_data, None, temperature=3.0, use_context=True, use_gumbel_softmax=True)
    
        abx_feature = query

        #print(abx_feature)
        #time.sleep(1.0)

        if self.get_encoded:
            abx_feature = encodedData
        if self.seq_norm:
            mean = abx_feature.mean(dim=1, keepdim=True)
            var = abx_feature.var(dim=1, keepdim=True)
            abx_feature = (abx_feature - mean) / torch.sqrt(var + 1e-08)
        return abx_feature

    def cuda(self):
        self.is_cuda = True
        super(FeatureModule, self).cuda()

    def cpu(self):
        self.is_cuda = False
        super(FeatureModule, self).cuda()

    def get_output_dim(self):
        if self.get_encoded:
            return self.config["hiddenEncoder"]
        return self.config["hiddenGar"]


class ModelPhoneCombined(torch.nn.Module):
    r"""
    Concatenates a CPC feature maker and a phone predictor.
    """

    def __init__(self, model, criterion, oneHot):
        r"""
        Arguments:
            model (FeatureModule): feature maker
            criterion (PhoneCriterion): phone predictor
            oneHot (bool): set to True to get a one hot output
        """
        super(ModelPhoneCombined, self).__init__()
        self.model = model
        self.criterion = criterion
        self.oneHot = oneHot

    def getDownsamplingFactor(self):
        return self.model.getDownsamplingFactor()

    def forward(self, data):
        c_feature = self.model(data)
        pred = self.criterion.getPrediction(c_feature)
        P = pred.size(2)

        if self.oneHot:
            pred = pred.argmax(dim=2)
            pred = toOneHot(pred, P)
        else:
            pred = torch.nn.functional.softmax(pred, dim=2)
        return pred


def loadArgs(args, locArgs, forbiddenAttr=None):
    for k, v in vars(locArgs).items():
        if forbiddenAttr is not None:
            if k not in forbiddenAttr:
                setattr(args, k, v)
        else:
            setattr(args, k, v)


def loadSupervisedCriterion(pathCheckpoint):
    from .criterion import CTCPhoneCriterion, PhoneCriterion

    *_, args = getCheckpointData(os.path.dirname(pathCheckpoint))
    _, nPhones = parseSeqLabels(args.pathPhone)
    if args.CTC:
        criterion = CTCPhoneCriterion(args.hiddenGar if not args.onEncoder
                                      else args.hiddenEncoder,
                                      nPhones, args.onEncoder)
    else:
        criterion = PhoneCriterion(args.hiddenGar, nPhones, args.onEncoder)

    state_dict = torch.load(pathCheckpoint)
    criterion.load_state_dict(state_dict["cpcCriterion"])
    return criterion, nPhones


def getCheckpointData(pathDir):
    if not os.path.isdir(pathDir):
        return None
    checkpoints = [x for x in os.listdir(pathDir)
                   if os.path.splitext(x)[1] == '.pt'
                   and os.path.splitext(x[11:])[0].isdigit()]
    if len(checkpoints) == 0:
        print("No checkpoints found at " + pathDir)
        return None
    checkpoints.sort(key=lambda x: int(os.path.splitext(x[11:])[0]))
    data = os.path.join(pathDir, checkpoints[-1])
    with open(os.path.join(pathDir, 'checkpoint_logs.json'), 'rb') as file:
        logs = json.load(file)

    with open(os.path.join(pathDir, "config.json" ), 'rb') as file:
        args = json.load(file)

    args = argparse.Namespace(**args)
    defaultArgs = get_default_cpc_config()
    loadArgs(defaultArgs, args)

    return os.path.abspath(data), logs, defaultArgs


def getEncoder(args):

    if args.encoder_type == 'mfcc':
        from .model import MFCCEncoder
        return MFCCEncoder(args.hiddenEncoder)
    elif args.encoder_type == 'lfb':
        from .model import LFBEnconder
        return LFBEnconder(args.hiddenEncoder)
    else:
        from .model import CPCEncoder
        return CPCEncoder(args.hiddenEncoder, args.normMode)


def getAR(args):
    if args.arMode == 'transformer':
        from .transformers import buildTransformerAR
        arNet = buildTransformerAR(args.hiddenEncoder, 1,
                                   args.sizeWindow // 160, args.abspos)
        args.hiddenGar = args.hiddenEncoder
    elif args.arMode == 'no_ar':
        from .model import NoAr
        arNet = NoAr()
    else:
        from .model import CPCAR
        arNet = CPCAR(args.hiddenEncoder, args.hiddenGar,
                      args.samplingType == "sequential",
                      args.nLevelsGRU,
                      mode=args.arMode,
                      reverse=args.cpc_mode == "reverse")
    return arNet

def loadModel(path, loadStateDict=True):
    print(f"Loading checkpoint {path}")
    pathCheckpoint, _, locArgs = getCheckpointData(os.path.dirname(path))

    print(f"Found checkpoint {pathCheckpoint}")

    print("Model params:")
    print(locArgs)

    state_dict = torch.load(pathCheckpoint, 'cpu')

    spModel = sparsespeech2.SparseSpeechModel(sizeWindow=locArgs.sizeWindow, EncoderDecoderType=locArgs.encoderDecoder,
                                              hiddenDim=locArgs.hiddenEncoder, rawEncodedDim=locArgs.hiddenGar,
                                              nLevels=locArgs.nLevels, abspos=locArgs.abspos,
                                              memory_n=40,
                                              num_heads_transformer=locArgs.nHeads, maskingProb=0.)

    spModel.load_state_dict(state_dict["gEncoder"], strict=False)

    return spModel, locArgs

def loadModelConcatenated(pathCheckpoints, loadStateDict=True):
    models = []
    hiddenGar, hiddenEncoder = 0, 0
    for path in pathCheckpoints:
        print(f"Loading checkpoint {path}")
        _, _, locArgs = getCheckpointData(os.path.dirname(path))

        doLoad = locArgs.load is not None and \
            (len(locArgs.load) > 1 or
             os.path.dirname(locArgs.load[0]) != os.path.dirname(path))

        if doLoad:
            m_, hg, he = loadModel(locArgs.load, loadStateDict=False)
            hiddenGar += hg
            hiddenEncoder += he
        else:
            encoderNet = getEncoder(locArgs)

            arNet = getAR(locArgs)
            m_ = SparseSpeechModel(encoderNet, arNet)

        if loadStateDict:
            print(f"Loading the state dict at {path}")
            state_dict = torch.load(path, 'cpu')
            m_.load_state_dict(state_dict["gEncoder"], strict=False)
        if not doLoad:
            hiddenGar += locArgs.hiddenGar
            hiddenEncoder += locArgs.hiddenEncoder

        models.append(m_)

    if len(models) == 1:
        return models[0], hiddenGar, hiddenEncoder

    return ConcatenatedModel(models), hiddenGar, hiddenEncoder


def get_module(i_module):
    if isinstance(i_module, torch.nn.DataParallel):
        return get_module(i_module.module)
    if isinstance(i_module, FeatureModule):
        return get_module(i_module.module)
    return i_module


def save_checkpoint(model_state, criterion_state, optimizer_state, best_state,
                    path_checkpoint):

    state_dict = {"gEncoder": model_state,
                  "cpcCriterion": criterion_state,
                  "optimizer": optimizer_state,
                  "best": best_state}

    torch.save(state_dict, path_checkpoint)


def toOneHot(inputVector, nItems):

    batchSize, seqSize = inputVector.size()
    out = torch.zeros((batchSize, seqSize, nItems),
                      device=inputVector.device, dtype=torch.long)
    out.scatter_(2, inputVector.view(batchSize, seqSize, 1), 1)
    return out


def seqNormalization(out):
    # out.size() = Batch x Seq x Channels
    mean = out.mean(dim=1, keepdim=True)
    var = out.var(dim=1, keepdim=True)
    return (out - mean) / torch.sqrt(var + 1e-08)


def buildFeature(featureMaker, seqPath, strict=False,
                 maxSizeSeq=64000, seqNorm=False):
    r"""
    Apply the featureMaker to the given file.
    Arguments:
        - featureMaker (FeatureModule): model to apply
        - seqPath (string): path of the sequence to load
        - strict (bool): if True, always work with chunks of the size
                         maxSizeSeq
        - maxSizeSeq (int): maximal size of a chunk
        - seqNorm (bool): if True, normalize the output along the time
                          dimension to get chunks of mean zero and var 1
    Return:
        a torch vector of size 1 x Seq_size x Feature_dim
    """
    seq = torchaudio.load(seqPath)[0]
    sizeSeq = seq.size(1)
    start = 0
    out = []
    while start < sizeSeq:
        if strict and start + maxSizeSeq > sizeSeq:
            break
        end = min(sizeSeq, start + maxSizeSeq)
        subseq = (seq[:, start:end]).view(1, 1, -1).cuda(device=0)
        with torch.no_grad():
            features = featureMaker((subseq, None))
            if seqNorm:
                features = seqNormalization(features)
        out.append(features.detach().cpu())
        start += maxSizeSeq

    if strict and start < sizeSeq:
        subseq = (seq[:, -maxSizeSeq:]).view(1, 1, -1).cuda(device=0)
        with torch.no_grad():
            features = featureMaker((subseq, None))
            if seqNorm:
                features = seqNormalization(features)
        delta = (sizeSeq - start) // featureMaker.getDownsamplingFactor()
        out.append(features[:, -delta:].detach().cpu())

    out = torch.cat(out, dim=1)
    return out

def build_feature_from_file(file_path, feature_maker, max_size_seq=64000):
    r"""
    Apply the featureMaker to the given file.
    Arguments:
        - file_path (FeatureModule): model to apply
        - file_path (string): path of the sequence to load
        - seq_norm (bool): if True, normalize the output along the time
                           dimension to get chunks of mean zero and var 1
        - max_size_seq (int): maximal size of a chunk
    Return:
        a torch vector of size 1 x Seq_size x Feature_dim
    """
    seq = torchaudio.load(file_path)[0]
    sizeSeq = seq.size(1)
    start = 0
    out = []
    while start < sizeSeq:
        if start + max_size_seq > sizeSeq:
            break
        end = min(sizeSeq, start + max_size_seq)
        subseq = (seq[:, start:end]).view(1, 1, -1).cuda(device=0)
        with torch.no_grad():
            features = feature_maker(subseq)
        out.append(features.detach().cpu())
        start += max_size_seq

    if start < sizeSeq:
        subseq = (seq[:, -max_size_seq:]).view(1, 1, -1).cuda(device=0)
        with torch.no_grad():
            features = feature_maker(subseq)
        df = subseq.size(2) // features.size(1)
        delta = (sizeSeq - start) // df
        out.append(features[:, -delta:].detach().cpu())

    out = torch.cat(out, dim=1)
    return out.view(out.size(1), out.size(2))
