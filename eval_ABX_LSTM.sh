FEATURE=LSTM2X_dense
DB_NAME=dev-clean
EXTENSION=.flac
FEATURE_SIZE=0.01
#METRIC=cosine
METRIC=kl_symmetric

BASEDIR=/raid/milde
BASEDIR=/scratch


#PATH_AUDIO_DIR=/scratch/librilight/sets/LibriSpeech/$DB_NAME
PATH_AUDIO_DIR=$BASEDIR/libri-light/unlab_data/LibriSpeech/$DB_NAME
PATH_TO_ABX_ITEMS=$BASEDIR/libri-light/unlab_data/ABX_data/
#/scratch/librilight/ABX_data

PATH_OUTPUT_CHECKPOINT=./models/1631099782/

# This one is the same as 1631099782, but with the ~~phase~~ complex removed
PATH_OUTPUT_CHECKPOINT=./models/1639063320/

# This one is the same as 1631099782, but with phase removed (fixed)
PATH_OUTPUT_CHECKPOINT=./models/1648121760/

#PATH_OUTPUT_CHECKPOINT=./models/1628616366/

#for DB_NAME in dev-clean dev-other test-clean test-other; do
        OUTPUT_DIR=evals/eval_${FEATURE}_${DB_NAME}_${METRIC}/
        mkdir -p $OUTPUT_DIR
        echo "computing ABX measure for $DB_NAME. Putting results in $OUTPUT_DIR."
        # default --max_size_group 10
        CUDA_VISIBLE_DEVICES=0 python3 eval_ABX.py $PATH_AUDIO_DIR  $PATH_TO_ABX_ITEMS/$DB_NAME.item --file_extension $EXTENSION --out $OUTPUT_DIR --feature_size $FEATURE_SIZE --distance_mode $METRIC --path_checkpoint $PATH_OUTPUT_CHECKPOINT --cuda #--max_size_group 10 --max_x_across 5 
#done

