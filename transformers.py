# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.
import torch
import torch.nn as nn
from torch.nn import Parameter
import math
import torch.nn.functional as F

class IDModule(nn.Module):

    def __init__(self, *args, **kwargs):
        super(IDModule, self).__init__()

    def forward(self, x):
        return x

# from https://github.com/tnq177/transformers_without_tears/blob/master/layers.py
class ScaleNorm(nn.Module):
    """ScaleNorm"""
    def __init__(self, scale, eps=1e-5):
        super(ScaleNorm, self).__init__()
        self.scale = Parameter(torch.tensor(scale))
        self.eps = eps

    def forward(self, x):
        norm = self.scale / torch.norm(x, dim=-1, keepdim=True).clamp(min=self.eps)
        return x * norm

class MultiheadAttentionWT(nn.Module):
    """
    MultiheadAttention module
    I learned a lot from https://github.com/pytorch/fairseq/blob/master/fairseq/modules/multihead_attention.py
    """
    def __init__(self, embed_dim, num_heads, att_dropout, use_bias):
        super(MultiheadAttentionWT, self).__init__()
        self.embed_dim = embed_dim
        self.num_heads = num_heads
        self.dropout = att_dropout
        self.use_bias = use_bias

        if self.embed_dim % self.num_heads != 0:
            raise ValueError("Required: embed_dim % num_heads == 0")

        self.head_dim = self.embed_dim // self.num_heads
        self.scale = self.head_dim ** -0.5

        # Parameters for linear projections of queries, keys, values and output
        self.weights = Parameter(torch.Tensor(4 * self.embed_dim, self.embed_dim))
        if self.use_bias:
            self.biases = Parameter(torch.Tensor(4 * self.embed_dim))

        # initializing
        # If we do Xavier normal initialization, std = sqrt(2/(2D))
        # but it's too big and causes unstability in PostNorm
        # so we use the smaller std of feedforward module, i.e. sqrt(2/(5D))
        mean = 0
        std = (2 / (5 * self.embed_dim)) ** 0.5
        nn.init.normal_(self.weights, mean=mean, std=std)
        if self.use_bias:
            nn.init.constant_(self.biases, 0.)

    def forward(self, q, k, v, mask, do_proj_qkv=True):
        def _split_heads(tensor):
            bsz, length, embed_dim = tensor.size()
            tensor = tensor.reshape(bsz, length, self.num_heads,
                                    self.head_dim).transpose(1, 2).reshape(bsz * self.num_heads, -1, self.head_dim)
            return tensor

        if do_proj_qkv:
            q, k, v = self.proj_qkv(q, k, v)

        q = _split_heads(q)
        k = _split_heads(k)
        v = _split_heads(v)

        att_weights = torch.bmm(q, k.transpose(1, 2)) * self.scale
        bsz_x_num_heads, src_len, tgt_len = att_weights.size()
        bsz = bsz_x_num_heads // self.num_heads
        att_weights = att_weights.reshape(bsz, self.num_heads, src_len, tgt_len)
        if mask is not None:
            att_weights.masked_fill_(mask, -1e9)

        att_weights = F.softmax(att_weights, dim=-1)
        att_weights = F.dropout(att_weights, p=self.dropout, training=self.training)
        _att_weights = att_weights.reshape(bsz_x_num_heads, src_len, tgt_len)
        output = torch.bmm(_att_weights, v)
        output = output.reshape(bsz, self.num_heads, src_len, self.head_dim).transpose(1, 2).reshape(bsz, src_len, -1)
        output = self.proj_o(output)

        return output, att_weights

    def proj_qkv(self, q, k, v):
        qkv_same = q.data_ptr() == k.data_ptr() == v.data_ptr()
        kv_same = k.data_ptr() == v.data_ptr()

        if qkv_same:
            q, k, v = self._proj(q, end=3 * self.embed_dim).chunk(3, dim=-1)
        elif kv_same:
            q = self._proj(q, end=self.embed_dim)
            k, v = self._proj(k, start=self.embed_dim, end=3 * self.embed_dim).chunk(2, dim=-1)
        else:
            q = self.proj_q(q)
            k = self.proj_k(k)
            v = self.proj_v(v)

        return q, k, v

    def _proj(self, x, start=0, end=None):
        weight = self.weights[start:end, :]
        bias = None if not self.use_bias else self.biases[start:end]
        return F.linear(x, weight=weight, bias=bias)

    def proj_q(self, q):
        return self._proj(q, end=self.embed_dim)

    def proj_k(self, k):
        return self._proj(k, start=self.embed_dim, end=2 * self.embed_dim)

    def proj_v(self, v):
        return self._proj(v, start=2 * self.embed_dim, end=3 * self.embed_dim)

    def proj_o(self, x):
        return self._proj(x, start=3 * self.embed_dim)


class FeedForwardWT(nn.Module):
    """FeedForward"""
    def __init__(self, ff_dim, embed_dim, ff_dropout=0.0, use_bias=True):
        super(FeedForwardWT, self).__init__()
        self.dropout = ff_dropout
        self.ff_dim = ff_dim
        self.embed_dim = embed_dim
        self.use_bias = use_bias

        self.in_proj = nn.Linear(self.embed_dim, self.ff_dim, bias=self.use_bias)
        self.out_proj = nn.Linear(self.ff_dim, self.embed_dim, bias=self.use_bias)

        # initializing
        mean = 0
        std = (2 / (self.ff_dim + self.embed_dim)) ** 0.5
        nn.init.normal_(self.in_proj.weight, mean=mean, std=std)
        nn.init.normal_(self.out_proj.weight, mean=mean, std=std)
        if self.use_bias:
            nn.init.constant_(self.in_proj.bias, 0.)
            nn.init.constant_(self.out_proj.bias, 0.)

    def forward(self, x):
        # my preliminary experiments show all RELU-variants
        # work the same and slower, RELU FTW!!!
        y = F.relu(self.in_proj(x))
        y = F.dropout(y, p=self.dropout, training=self.training)
        return self.out_proj(y)

# args: {dropout, num_layers, pre_act, embed_dim, num_heads, att_dropout, use_bias, ff_dropout, ff_dim, scnorm}
class TransformerWT(nn.Module):
    """Self-attention Encoder"""
    def __init__(self, embed_dim, num_layers, num_heads=16, pre_act=True, ff_dim=0,
                 use_bias=True, scnorm=True, ff_dropout=.1, att_dropout=.1, dropout=.1,
                 input_projection=0, output_projection=0):
        super(TransformerWT, self).__init__()

        self.num_layers = num_layers
        self.pre_act = pre_act
        self.num_heads = num_heads
        self.use_bias = use_bias

        if ff_dim==0:
            ff_dim = embed_dim*4

        self.ff_dim = ff_dim
        self.embed_dim = embed_dim

        self.dropout = dropout
        self.ff_dropout = ff_dropout
        self.att_dropout = att_dropout

        self.input_projection = input_projection
        self.output_projection = output_projection

        self.atts = nn.ModuleList([MultiheadAttentionWT(embed_dim=self.embed_dim, num_heads=self.num_heads,
                                                        att_dropout=self.att_dropout, use_bias=self.use_bias)
                                   for _ in range(self.num_layers)])

        self.ffs = nn.ModuleList([FeedForwardWT(ff_dim=self.ff_dim, embed_dim=self.embed_dim,
                                                ff_dropout=self.ff_dropout, use_bias=self.use_bias)
                                  for _ in range(self.num_layers)])

        num_scales = self.num_layers * 2 + 1 if self.pre_act else self.num_layers * 2
        if scnorm:
            self.scales = nn.ModuleList([ScaleNorm(self.embed_dim ** 0.5) for _ in range(num_scales)])
        else:
            self.scales = nn.ModuleList([nn.LayerNorm(self.embed_dim) for _ in range(num_scales)])

        if self.input_projection > 0:
            self.proj_in = nn.Linear(self.input_projection, self.embed_dim, bias=self.use_bias)

        if self.output_projection > 0:
            self.proj_out = nn.Linear(self.embed_dim, self.output_projection, bias=self.use_bias)

    def forward(self, src_inputs, src_mask=None):
        pre_act = self.pre_act
        post_act = not pre_act

        if self.input_projection > 0:
            src_inputs = self.proj_in(src_inputs)

        x = F.dropout(src_inputs, p=self.dropout, training=self.training)
        for i in range(self.num_layers):
            att = self.atts[i]
            ff = self.ffs[i]
            att_scale = self.scales[2 * i]
            ff_scale = self.scales[2 * i + 1]

            residual = x
            x = att_scale(x) if pre_act else x
            x, _ = att(q=x, k=x, v=x, mask=src_mask)
            x = residual + F.dropout(x, p=self.dropout, training=self.training)
            x = att_scale(x) if post_act else x

            residual = x
            x = ff_scale(x) if pre_act else x
            x = ff(x)
            x = residual + F.dropout(x, p=self.dropout, training=self.training)
            x = ff_scale(x) if post_act else x

        x = self.scales[-1](x) if pre_act else x

        if self.output_projection > 0:
            x = self.proj_out(x)

        return x

class ScaledDotProductAttention(nn.Module):
    def __init__(self,
                 sizeSeq,         # Size of the input sequence
                 dk,              # Dimension of the input sequence
                 dropout,         # Dropout parameter
                 relpos=False,    # Do we retrieve positional information ?
                 useMask=False):
        super(ScaledDotProductAttention, self).__init__()

        self.drop = nn.Dropout(dropout)
        self.softmax = nn.Softmax(dim=2)
        self.relpos = relpos
        self.sizeSeq = sizeSeq

        if relpos:
            self.Krelpos = nn.Parameter(torch.Tensor(dk, sizeSeq))
            self.initmat_(self.Krelpos)
            self.register_buffer('z', torch.zeros(1, sizeSeq, 1))

        self.useMask = useMask

        if useMask:
            # A mask is set so that a node never queries data in the future
            mask = torch.tril(torch.ones(sizeSeq, sizeSeq), diagonal=0)
            mask = 1 - mask
            mask[mask == 1] = -float('inf')
            self.register_buffer('mask', mask.unsqueeze(0))

    def initmat_(self, mat, dim=0):
        stdv = 1. / math.sqrt(mat.size(dim))
        mat.data.uniform_(-stdv, stdv)

    def forward(self, Q, K, V):
        # Input dim : N x sizeSeq x dk
        QK = torch.bmm(Q, K.transpose(-2, -1))

        if self.relpos:
            bsz = Q.size(0)
            QP = Q.matmul(self.Krelpos)
            # This trick with z fills QP's diagonal with zeros
            QP = torch.cat((self.z.expand(bsz, -1, -1), QP), 2)
            QK += QP.view(bsz, self.sizeSeq + 1, self.sizeSeq)[:, 1:, :]
        A = self.softmax(QK / math.sqrt(K.size(-1)) )

        if self.useMask:
            A += self.mask

        return torch.bmm(self.drop(A), V)


class MultiHeadAttention(nn.Module):
    def __init__(self,
                 sizeSeq,   # Size of a sequence
                 dropout,   # Dropout parameter
                 dmodel,    # Model's dimension
                 nheads,    # Number of heads in the model
                 abspos,
                 smallInit=False):   # Is positional information encoded in the input ?
        super(MultiHeadAttention, self).__init__()
        self.Wo = nn.Linear(dmodel, dmodel, bias=False)
        self.Wk = nn.Linear(dmodel, dmodel, bias=False)
        self.Wq = nn.Linear(dmodel, dmodel, bias=False)
        self.Wv = nn.Linear(dmodel, dmodel, bias=False)
        self.nheads = nheads
        self.dk = dmodel // nheads
        self.Att = ScaledDotProductAttention(sizeSeq, self.dk,
                                             dropout, not abspos)

        if smallInit:
            # non-default initializing, with smaller weights:
            # If we do Xavier normal initialization, std = sqrt(2/(2D))
            # but it's too big and causes unstability in PostNorm
            # so we use the smaller std of feedforward module, i.e. sqrt(2/(5D))

            mean = 0
            std = (2 / (5 * dmodel)) ** 0.5
            nn.init.normal_(self.Wo.weight, mean=mean, std=std)
            nn.init.normal_(self.Wk.weight, mean=mean, std=std)
            nn.init.normal_(self.Wq.weight, mean=mean, std=std)
            nn.init.normal_(self.Wv.weight, mean=mean, std=std)


    def trans_(self, x):
        bsz, bptt, h, dk = x.size(0), x.size(1), self.nheads, self.dk
        return x.view(bsz, bptt, h, dk).transpose(1, 2).contiguous().view(bsz * h, bptt, dk)

    def reverse_trans_(self, x):
        bsz, bptt, h, dk = x.size(
            0) // self.nheads, x.size(1), self.nheads, self.dk
        return x.view(bsz, h, bptt, dk).transpose(1, 2).contiguous().view(bsz, bptt, h * dk)

    def forward(self, Q, K, V):
        q = self.trans_(self.Wq(Q))
        k = self.trans_(self.Wk(K))
        v = self.trans_(self.Wv(V))
        y = self.reverse_trans_(self.Att(q, k, v))
        return self.Wo(y)


class FFNetwork(nn.Module):
    def __init__(self, din, dout, dff, dropout, smallInit=False):
        super(FFNetwork, self).__init__()
        self.lin1 = nn.Linear(din, dff, bias=True)
        self.lin2 = nn.Linear(dff, dout, bias=True)
        self.relu = nn.ReLU()
        self.drop = nn.Dropout(dropout)

        if smallInit:
            mean = 0
            std = (2 / (5 * dout)) ** 0.5
            nn.init.normal_(self.lin1.weight, mean=mean, std=std)
            nn.init.normal_(self.lin2.weight, mean=mean, std=std)

    def forward(self, x):
        return self.lin2(self.drop(self.relu(self.lin1(x))))


class TransformerLayer(nn.Module):
    def __init__(self, sizeSeq=32, dmodel=512, dff=2048,
                 dropout=0.1, nheads=8,
                 abspos=False, norm='scalenorm'):
        super(TransformerLayer, self).__init__()
        self.multihead = MultiHeadAttention(sizeSeq, dropout,
                                            dmodel, nheads, abspos)
        if norm=='layernorm':
            self.ln_multihead = nn.LayerNorm(dmodel)
            self.ln_ffnetwork = nn.LayerNorm(dmodel)
        elif norm=='scalenorm':
            self.ln_multihead = ScaleNorm(dmodel**0.5)
            self.ln_ffnetwork = ScaleNorm(dmodel**0.5)
        else:
            self.ln_multihead = IDModule
            self.ln_ffnetwork = IDModule

        self.ffnetwork = FFNetwork(dmodel, dmodel, dff, dropout)

    # this is the classic version, layer norm last
    #def forward(self, x):
    #    y = self.ln_multihead(x + self.multihead(Q=x, K=x, V=x))
    #    return self.ln_ffnetwork(y + self.ffnetwork(y))

    # We are using pre layer norm. seems important:
    # https://tnq177.github.io/data/transformers_without_tears.pdf
    # see e.g. https://github.com/tnq177/witwicky
    # but its non-standard
    def forward(self, x):
        #print("x.size():", x.size())

        x_ln = self.ln_multihead(x)
        y = x + self.multihead(Q=x_ln, K=x_ln, V=x_ln)

        #y = x + self.ln_multihead(self.multihead(Q=x, K=x, V=x))

        ret = y + self.ffnetwork(self.ln_ffnetwork(y))

        #print("ret.size():", ret.size())

        return ret


class StaticPositionEmbedding(nn.Module):
    def __init__(self, seqlen, dmodel):
        super(StaticPositionEmbedding, self).__init__()
        pos = torch.arange(0., seqlen).unsqueeze(1).repeat(1, dmodel)
        dim = torch.arange(0., dmodel).unsqueeze(0).repeat(seqlen, 1)
        div = torch.exp(- math.log(10000) * (2*(dim//2)/dmodel))
        pos *= div
        pos[:, 0::2] = torch.sin(pos[:, 0::2])
        pos[:, 1::2] = torch.cos(pos[:, 1::2])
        self.register_buffer('pe', pos.unsqueeze(0))

    def forward(self, x):
        #print(x.size())
        return x + self.pe[:, :x.size(1), :]


def buildTransformerAR(sizeSeq,       # Expected size of the input sequence
                       dimEncoded,    # Output dimension of the encoder
                       nLayers,       # Number of transformer layers
                       abspos,
                       nheads=16,
                       input_projection=None):
    layerSequence = []

    print("Creating transformer network with nHeads",nheads,"and nLayers",nLayers,"and dimension",dimEncoded,'input_projection',input_projection)

    if input_projection is not None:
        #layerSequence += [nn.LayerNorm(input_projection)]
        #layerSequence += [FFNetwork(input_projection, dimEncoded, dff=2048, dropout=.1)]
        layerSequence += [nn.Linear(input_projection, dimEncoded, bias=True)]

    if abspos:
        layerSequence += [StaticPositionEmbedding(sizeSeq, dimEncoded)]
    layerSequence += [TransformerLayer(sizeSeq=sizeSeq,
                                       dmodel=dimEncoded,nheads=nheads, abspos=abspos)
                      for i in range(nLayers)]

    #layerSequence += [nn.Linear(dimEncoded, dimEncoded, bias=True)]

    return nn.Sequential(*layerSequence)
