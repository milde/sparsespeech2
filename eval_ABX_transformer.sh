
DB_NAME=dev-clean
EXTENSION=.flac
FEATURE_SIZE=0.01
METRIC=kl_symmetric
#METRIC=cosine

PATH_AUDIO_DIR=/scratch/librilight/sets/LibriSpeech/$DB_NAME

PATH_TO_ABX_ITEMS=/scratch/librilight/ABX_data

PATH_OUTPUT_CHECKPOINT=models_ltgpu/transformer_1/

#for DB_NAME in dev-clean dev-other test-clean test-other; do
        OUTPUT_DIR=evals/eval_${FEATURE}_${DB_NAME}_${METRIC}/
        mkdir -p $OUTPUT_DIR
        echo "computing ABX measure for $DB_NAME. Putting results in $OUTPUT_DIR."
        # default --max_size_group 10
        python3 eval_ABX.py $PATH_AUDIO_DIR  $PATH_TO_ABX_ITEMS/$DB_NAME.item --file_extension $EXTENSION --out $OUTPUT_DIR --feature_size $FEATURE_SIZE --distance_mode $METRIC --path_checkpoint $PATH_OUTPUT_CHECKPOINT --cuda #--max_size_group 10 --max_x_across 5 
#done

