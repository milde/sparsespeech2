import argparse

import torch
import torchaudio
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import Parameter
from stgumbel import gumbel_softmax

import sys

from transformers import buildTransformerAR, TransformerWT

class IDModule(nn.Module):

    def __init__(self, *args, **kwargs):
        super(IDModule, self).__init__()

    def forward(self, x):
        return x

# from https://github.com/tnq177/transformers_without_tears/blob/master/layers.py
class ScaleNorm(nn.Module):
    """ScaleNorm"""
    def __init__(self, scale, eps=1e-5):
        super(ScaleNorm, self).__init__()
        self.scale = Parameter(torch.tensor(scale))
        self.eps = eps

    def forward(self, x):
        norm = self.scale / torch.norm(x, dim=-1, keepdim=True).clamp(min=self.eps)
        return x * norm

class BaseCriterion(nn.Module):

    def warmUp(self):
        return False

    def update(self):
        return


class MelCriterion(BaseCriterion):
    def __init__(self, dimEncoder, n_mels=20):
        super(MelCriterion, self).__init__()
        self.spec = torchaudio.transforms.Spectrogram(n_fft=400, hop_length=160)
        self.melscale = torchaudio.transforms.MelScale(n_mels=n_mels, sample_rate = 16000, n_stft = 201, mel_scale = 'htk')
        self.huber_loss_mel = torch.nn.SmoothL1Loss(reduction='mean')

    def forward(self, batchData, encodedData, cFeature, cOutput, decodedData):

#        batchDataFFT = torch.stft(torch.squeeze(batchData), n_fft=201,
#                                    hop_length=160, return_complex=False)
#
#        decodedDataFFT = torch.stft(torch.squeeze(decodedData), n_fft=201,
#                                        hop_length=160, return_complex=False)

        batchDataSpec = self.spec(torch.squeeze(batchData))
        decodedDataSpec = self.spec(torch.squeeze(decodedData))

        batchDataMel = self.melscale(batchDataSpec)
        decodedDataMel = self.melscale(decodedDataSpec)

        mel_loss = self.huber_loss_mel(batchDataMel, decodedDataMel)

        return mel_loss


class FFTCriterion(BaseCriterion):

    def __init__(self, dimEncoder):
        super(FFTCriterion, self).__init__()

        self.dimEncoder = dimEncoder

        self.huber_loss = torch.nn.SmoothL1Loss(reduction='mean')
        self.huber_loss_fft = torch.nn.SmoothL1Loss(reduction='mean')
        self.mel_scale = torchaudio.transforms.MelScale(n_stft=512, n_mels=40, sample_rate=16000)

        self.ignore_complex = False
        self.ignore_phase = False
        self.apply_mel_scale = False

    def forward(self, batchData, encodedData, cFeature, cOutput, decodedData):

        # cFeature.size() : batchSize x seq Size x hidden size
        #batchSize = cFeature.size(0)
        #cFeature = cFeature[:, -1, :]
        #cFeature = cFeature.view(batchSize, -1)
        #predictions = self.linearSpeakerClassifier(cFeature)

        #loss = self.lossCriterion(predictions, label).view(1, -1)
        #acc = (predictions.max(1)[1] == label).double().mean().view(1, -1)

        #print('batchData.size():', batchData.size())
        #print('decodedData.size():', decodedData.size())

        #batchData.size(): torch.Size([8, 1, 81920])
        #decodedData.size(): torch.Size([8, 1, 81920])

        batchDataFFT = torch.stft(torch.squeeze(batchData), n_fft=512, 
                                    hop_length=160, return_complex=False)

        decodedDataFFT = torch.stft(torch.squeeze(decodedData), n_fft=512,
                                        hop_length=160, return_complex=False)

        #print('batchDataFFT.size():', batchDataFFT.size())
        #print('decodedDataFFT.size():', decodedDataFFT.size())
        
        if self.ignore_phase:
            if len(batchDataFFT.shape) == 4:

                batch_re = batchDataFFT[:,:,:,0]
                batch_img = batchDataFFT[:,:,:,1]

                decoded_re = decodedDataFFT[:,:,:,0]
                decoded_img = decodedDataFFT[:,:,:,1]

                batchDataFFT2 = batch_re*batch_re + batch_img*batch_img
                decodedDataFFT2 = decoded_re*decoded_re + decoded_img*decoded_img

            elif len(batchDataFFT.shape) == 3:
                
                batch_re = batchDataFFT[:,:,0]
                batch_img = batchDataFFT[:,:,1]

                decoded_re = decodedDataFFT[:,:,0]
                decoded_img = decodedDataFFT[:,:,1]

                batchDataFFT2 = batch_re*batch_re + batch_img*batch_img
                decodedDataFFT2 = decoded_re*decoded_re + decoded_img*decoded_img
            
            else:
                print('Warning len(batchDataFFT.shape) is neither 3 nor 4:', len(batchDataFFT.shape))
        elif self.ignore_complex:
            # ignore complex part of the fft
            if len(batchDataFFT.shape) == 4:
                batchDataFFT2 = batchDataFFT[:,:,:,0]
                decodedDataFFT2 = decodedDataFFT[:,:,:,0]
            elif len(batchDataFFT.shape) == 3:
                batchDataFFT2 = batchDataFFT[:,:,0]
                decodedDataFFT2 = decodedDataFFT[:,:,0]
            else:
                print('Warning len(batchDataFFT.shape) is neither 3 nor 4:', len(batchDataFFT.shape))
        else:
            batchDataFFT2 = batchDataFFT
            decodedDataFFT2 = decodedDataFFT

        #print('batchDataFFT.size():', batchDataFFT.size())
        #print('decodedDataFFT.size():', decodedDataFFT.size())

        if self.apply_mel_scale:
            batchDataFFT2 = self.mel_scale(batchDataFFT)
            decodedDataFFT2 = self.mel_scale(decodedDataFFT)

        #print('batchDataFFT2.size():', batchDataFFT2.size())
        #print('decodedDataFFT2.size():', decodedDataFFT2.size())

        #batchDataFFT.size(): torch.Size([8, 257, 513, 2])
        #decodedDataFFT.size(): torch.Size([8, 257, 513, 2])
        #batchDataFFT_mel.size(): torch.Size([8, 257, 40, 2])
        #decodedDataFFT_mel.size(): torch.Size([8, 257, 40, 2])

        #print('output:', decodedDataFFT.detach().cpu().numpy())

        fft_loss = self.huber_loss_fft(batchDataFFT2, decodedDataFFT2)

        #encoded_loss = self.huber_loss(cFeature, cOutput)
        #loss = fft_loss+encoded_loss
        #return loss, encoded_loss

        return fft_loss

class GumbelSoftmaxMemory(nn.Module):

    def __init__(self, inDim, memoryN, embDim):
        super(GumbelSoftmaxMemory, self).__init__()

        self.inDim = inDim
        self.embDim = embDim
        self.memoryN = memoryN

        self.input_proj = nn.Linear(inDim, memoryN, bias=True)

        print('Creating GumbelSoftmaxMemory with:', 'inDim:', inDim, 'memoryN:', memoryN, 'embDim:', embDim)

        self.value_memory = torch.nn.Parameter(torch.zeros(self.memoryN, self.embDim))

        mean = 0
        std = (2 / (self.memoryN + self.embDim)) ** 0.5

        nn.init.normal_(self.value_memory, mean=mean, std=std)


    def forward(self, x, temperature):
        #print('x:',x.size())

        logits = self.input_proj(x)
        query = gumbel_softmax(logits, temperature)

        #print('query:', x.size())

        #array_ops.expand_dims(value_memory, 0) * array_ops.expand_dims(context_out, -1)
        # context_mem_out = math_ops.reduce_sum(context_mem_out, [1])

        a = torch.unsqueeze(self.value_memory, 0)

        # expand memory to batch size of query (expand does not copy, its just a view)
        # a = a.expand(query.size()[0], -1, -1)

        b = torch.unsqueeze(query, -1)

        #print('value_memory:', a.size())
        #print('query:', b.size())

        out = a * b

        #print('out:', out.size())

        out_summed = torch.sum(out, 2)

        #print('out summed:',  out_summed.size())

        return out_summed, query, logits


class ChannelNorm(nn.Module):

    def __init__(self,
                 numFeatures,
                 epsilon=1e-05,
                 affine=True):

        super(ChannelNorm, self).__init__()
        if affine:
            self.weight = nn.parameter.Parameter(
                torch.Tensor(1, numFeatures, 1))
            self.bias = nn.parameter.Parameter(torch.Tensor(1, numFeatures, 1))
        else:
            self.weight = None
            self.bias = None
        self.epsilon = epsilon
        self.p = 0
        self.affine = affine
        self.reset_parameters()

    def reset_parameters(self):
        if self.affine:
            torch.nn.init.ones_(self.weight)
            torch.nn.init.zeros_(self.bias)

    def forward(self, x):

        cumMean = x.mean(dim=1, keepdim=True)
        cumVar = x.var(dim=1, keepdim=True)
        x = (x - cumMean)*torch.rsqrt(cumVar + self.epsilon)

        if self.weight is not None:
            x = x * self.weight + self.bias
        return x

# Encodes raw audio from the time domain
# This basically substitutes engineered features
class RawEncoder(nn.Module):

    def __init__(self,
                 sizeHidden=512,
                 normMode="layerNorm",
                 custom_init=True,
                 inputMask='forward',
                 maskSize=1024 * 8
                 ):

        super(RawEncoder, self).__init__()

        self.inputMask = inputMask
        self.maskSize = maskSize

        if self.inputMask == 'forward':
            print('[RawDecoder] using inputMask with size:', self.maskSize)

#        validModes = ["batchNorm", "instanceNorm", "ID", "layerNorm"]
#        if normMode not in validModes:
#            raise ValueError(f"Norm mode must be in {validModes}")
#
#        if normMode == "instanceNorm":
#            def normLayer(x): return nn.InstanceNorm1d(x, affine=True)
#        elif normMode == "ID":
#            normLayer = IDModule
#        elif normMode == "layerNorm":
#            normLayer = ChannelNorm
#        else:
#            normLayer = nn.BatchNorm1d

        normLayer = ScaleNorm #IDModule #ChannelNorm

        self.dimEncoded = sizeHidden
        self.conv0 = nn.Conv1d(1, sizeHidden, 10, stride=5, padding=3)
        self.conv1 = nn.Conv1d(sizeHidden, sizeHidden, 8, stride=4, padding=2)
        self.conv2 = nn.Conv1d(sizeHidden, sizeHidden, 4, stride=2, padding=1)
        self.conv3 = nn.Conv1d(sizeHidden, sizeHidden, 4, stride=2, padding=1)
        self.conv4 = nn.Conv1d(sizeHidden, sizeHidden, 4, stride=2, padding=1)

        if custom_init:
            mean = 0
            std = (1 / (sizeHidden*4)) ** 0.5

            nn.init.normal_(self.conv0.weight, mean=mean, std=std)
            nn.init.normal_(self.conv1.weight, mean=mean, std=std)
            nn.init.normal_(self.conv2.weight, mean=mean, std=std)
            nn.init.normal_(self.conv3.weight, mean=mean, std=std)
            nn.init.normal_(self.conv4.weight, mean=mean, std=std)

        normScale = sizeHidden ** 0.5
        self.batchNorm0 = normLayer(normScale)
        self.batchNorm1 = normLayer(normScale)
        self.batchNorm2 = normLayer(normScale)
        self.batchNorm3 = normLayer(normScale)
        self.batchNorm4 = normLayer(normScale)

        self.DOWNSAMPLING = 160

    def getDimOutput(self):
        return self.conv4.out_channels

    def forward(self, x):

        if self.inputMask == 'forward':
            net = x.clone()
            net[-self.maskSize:] = 0
        else:
            net = x

        net = F.gelu(self.batchNorm0(self.conv0(net)))
        net = F.gelu(self.batchNorm1(self.conv1(net)))
        net = F.gelu(self.batchNorm2(self.conv2(net)))
        net = F.gelu(self.batchNorm3(self.conv3(net)))
        #x = self.batchNorm4(self.conv4(x))
        net = F.tanh(self.batchNorm4(self.conv4(net)))
        return net

#torch.nn.ConvTranspose1d can be used as deconv
class RawDecoder(nn.Module):
    def __init__(self,
                 sizeHidden=512,
                 normMode="layerNorm"):

        super(RawDecoder, self).__init__()

        self.dimEncoded = sizeHidden
        self.normMode = normMode

        normLayer = IDModule #ScaleNorm #IDModule #ChannelNorm

        self.deconv0 = nn.ConvTranspose1d(sizeHidden, sizeHidden, 4, stride=2, padding=1)
        self.deconv1 = nn.ConvTranspose1d(sizeHidden, sizeHidden, 4, stride=2, padding=1)
        self.deconv2 = nn.ConvTranspose1d(sizeHidden, sizeHidden, 4, stride=2, padding=1)
        self.deconv3 = nn.ConvTranspose1d(sizeHidden, sizeHidden, 8, stride=4, padding=2)

        # For deconv4, see note on: https://pytorch.org/docs/stable/generated/torch.nn.ConvTranspose1d.html
        # The padding argument effectively adds dilation * (kernel_size - 1) - padding amount of zero padding to both
        # sizes of the input. This is set so that when a Conv1d and a ConvTranspose1d are initialized with same
        # parameters, they are inverses of each other in regard to the input and output shapes. However, when
        # stride > 1, Conv1d maps multiple input shapes to the same output shape. output_padding is provided
        # to resolve this ambiguity by effectively increasing the calculated output shape on one side. Note that
        # output_padding is only used to find output shape, but does not actually add zero-padding to output.

        self.deconv4 = nn.ConvTranspose1d(sizeHidden, 1, 10, stride=5, padding=3, output_padding=1)

        normScale = sizeHidden ** 0.5
        self.batchNorm0 = normLayer(normScale)
        self.batchNorm1 = normLayer(normScale)
        self.batchNorm2 = normLayer(normScale)
        self.batchNorm3 = normLayer(normScale)
        #self.batchNorm4 = normLayer(1**0.5)

    def forward(self, x):
        net = x

        net = F.gelu(self.batchNorm0(self.deconv0(net)))
#        print('x size:', x.size())
        net = F.gelu(self.batchNorm1(self.deconv1(net)))
#        print('x size:', x.size())
        net = F.gelu(self.batchNorm2(self.deconv2(net)))
#        print('x size:', x.size())
        net = F.gelu(self.batchNorm3(self.deconv3(net)))
#        print('x size:', x.size())
        net = F.tanh(self.deconv4(net))
#        print('x size:', x.size())
        return net

    def getDimOutput(self):
        return self.deconv4.out_channels

class SpRecurrentEncoderDecoder(nn.Module):

    def __init__(self,
                 dimEncoded,
                 dimOutput,
                 keepHidden,
                 nLevels,
                 bidirectional=True):

        super(SpRecurrentEncoderDecoder, self).__init__()
        self.baseNet = nn.LSTM(dimEncoded, dimOutput, bidirectional=bidirectional,
                               num_layers=nLevels, batch_first=True)
        self.hidden = None
        self.keepHidden = keepHidden
        self.bidirectional = bidirectional
        self.dimOutput = dimOutput
        self.dimEncoded = dimEncoded

    def getDimOutput(self):
        return self.baseNet.hidden_size

    def forward(self, x):

        inputSize = x.size()

        try:
            self.baseNet.flatten_parameters()
        except RuntimeError:
            pass
        x, h = self.baseNet(x, self.hidden)

        if self.keepHidden:
            if isinstance(h, tuple):
                self.hidden = tuple(x.detach() for x in h)
            else:
                self.hidden = h.detach()

        if self.bidirectional:
            #add forward and backward direction outputs to keep the dimension the same size as in the uni-directional case
            outputSize = x.size()
            #print('outputSize:', outputSize,self.dimOutput)
            x = x.reshape(outputSize[0], outputSize[1], 2, self.dimOutput)
            x = x[:, :, 0, :] + x[:, :, 1, :]

        return x

#class CPCModel(nn.Module):
#
#    def __init__(self,
#                 encoder,
#                 AR):
#
#        super(CPCModel, self).__init__()
#        self.gEncoder = encoder
#        self.gAR = AR
#
#    def forward(self, batchData, label):
#        encodedData = self.gEncoder(batchData).permute(0, 2, 1)
#        cFeature = self.gAR(encodedData)
#        return cFeature, encodedData, label

def dropout_mask(x, size, prob):
    """
    We pass size in so that we get broadcasting along the sequence dimension
    in RNNDropout.
    """
    return x.new(*size).bernoulli_(1-prob).div_(1-prob)

# Using the RNN dropout trick from https://people.ucsc.edu/~abrsvn/2_awd_lstm.html
class SequeunceDropout(nn.Module):
    """
    Note the way size is passed in the forward function: we insert a 3rd
    dimension
    (x.size(0), x.size(1), 1).

    the last dimension is the feature dimension (batch, seq, feature)
    """
    def __init__(self, prob=0.5):
        super().__init__()
        self.prob = prob

    def forward(self, x):
        if not self.training or self.prob == 0.:
            return x
        mask = dropout_mask(x.data, (x.size(0), x.size(1), 1), self.prob)
        return x * mask


class SparseSpeechModel(nn.Module):
    def __init__(self, sizeWindow, EncoderDecoderType='LSTM', hiddenDim=256, rawEncodedDim=512, nLevels=2,
                 maskingProb=0.20, abspos=False, num_heads_transformer=16, model_id=None,
                 memory_n=40, encoder_bottleneck_dim=40, inputMask='none', maskSize=0):
        super(SparseSpeechModel, self).__init__()

        self.memory_n = memory_n

        print('sizeWindow:', sizeWindow)

        self.raw_encoder = RawEncoder(sizeHidden=rawEncodedDim, inputMask=inputMask, maskSize=maskSize)

        self.model_id = str(model_id)

        self.maskingProb = maskingProb

        self.encoder_bottleneck = nn.Linear(rawEncodedDim, encoder_bottleneck_dim)
        self.encoder_bottleneck_proj = nn.Linear(encoder_bottleneck_dim, hiddenDim)

        context_dim=encoder_bottleneck_dim

        if EncoderDecoderType == 'LSTM':
            self.encoder = SpRecurrentEncoderDecoder(hiddenDim, hiddenDim, False, nLevels=nLevels)
        elif EncoderDecoderType == 'transformer':
            self.encoder = buildTransformerAR(sizeWindow // 160, rawEncodedDim, nLevels,  abspos)
        elif EncoderDecoderType == 'transformerWT':
            #{dropout, num_layers, pre_act, embed_dim, num_heads, att_dropout, use_bias, ff_dropout, ff_dim, scnorm}
            self.encoder = TransformerWT(hiddenDim, nLevels, num_heads=num_heads_transformer,
                                         input_projection=hiddenDim, output_projection=hiddenDim)
        else:
            print('EncoderDecoder type:', EncoderDecoderType, 'not available')
            sys.exit(-3)

        self.hiddenDim = hiddenDim
        self.rawEncodedDim = rawEncodedDim

        self.seqDropout = SequeunceDropout(prob=self.maskingProb)

        print('Created intermediate sequence mask between enc/dev with probability:', self.maskingProb)

        if EncoderDecoderType == 'LSTM':
            #innerdimension + contextvector of same size, thus its hiddenDim*2
            self.decoder = SpRecurrentEncoderDecoder(hiddenDim+context_dim, rawEncodedDim, False, nLevels=nLevels)
        elif EncoderDecoderType == 'transformer':
            self.decoder = buildTransformerAR(sizeWindow // 160, rawEncodedDim, nLevels,
                                              abspos, input_projection=rawEncodedDim*2)
        elif EncoderDecoderType == 'transformerWT':
            self.decoder = TransformerWT(hiddenDim, nLevels, num_heads=num_heads_transformer,
                                         input_projection=hiddenDim+context_dim, output_projection=rawEncodedDim)

        #self.decoder = SpRecurrentEncoderDecoder(hiddenDim, rawEncodedDim, False, nLevels=nLevels)
        self.raw_decoder = RawDecoder(sizeHidden=rawEncodedDim)

        self.gumbelmem = GumbelSoftmaxMemory(hiddenDim, memory_n, hiddenDim)

    def set_masking_prob(self, maskingProb):
        self.maskingProb = maskingProb
        self.seqDropout = SequeunceDropout(prob=self.maskingProb)

    def forward(self,  batchData, label, temperature = 1.0, use_context=True, use_gumbel_softmax=False, debug_print=False):
        #batch_first lstm – If True, then the input and output tensors are provided as (batch, seq, feature).
        # encoders start, input windows in the signal domain
        if debug_print:
            print('batch',batchData.size())

        raw_encodedData = self.raw_encoder(batchData)
        # Note: encoded that will be (batch, feat, seq), so we permute the last two

        if debug_print:
            print('encodedData raw', raw_encodedData.size())

        raw_encodedData = raw_encodedData.permute(0, 2, 1)

        # Now (batch, feature, seq)

        encodedData = self.encoder_bottleneck(raw_encodedData)

        encodedData_proj = self.encoder_bottleneck_proj(encodedData)

        cFeature_raw = self.encoder(encodedData_proj)

        if debug_print:
            print('cFeature raw', cFeature_raw.size())

        # encoders finished. Now we have the inner feature representation

        #context = cFeature_raw.mean(dim=1, keepdim=True).clone()
        context = encodedData.mean(dim=1, keepdim=True).clone()

        if use_gumbel_softmax:
            cFeature, query, logits = self.gumbelmem(cFeature_raw, temperature)
        else:
            cFeature = cFeature_raw
            query, logits = None, None

        # do sequence masking
        cFeature_seq_masked = self.seqDropout(cFeature)

        if use_context:
            inputSize = cFeature_seq_masked.size()
            #print('inputSize:', inputSize)
            #print('context dim:', context.size())
            context = context.repeat(1, inputSize[1], 1)
            #context = context.repeat(1, 1, inputSize[1])
            if debug_print:
                print('context dim:', context.size())
                print('cFeature dim:', cFeature.size())
            cFeature_cc_seq_masked = torch.cat((cFeature_seq_masked,context),2)
            if debug_print:
                print('cFeature dim after cat:', cFeature_cc_seq_masked.size())
        else:
            cFeature_cc_seq_masked = cFeature_seq_masked

        # decoders start
        cOutput = self.decoder(cFeature_cc_seq_masked)

        # raw decoder needs (batch, feat, seq), we permuted the inputs
        decodedData = self.raw_decoder(cOutput.permute(0, 2, 1))

        # decoders finished, we have a signal in the time domain again

        return raw_encodedData, encodedData, cFeature, cOutput, decodedData, query, cFeature_cc_seq_masked, cFeature_raw, logits

#def train():#
#
#    # Datasets
#    if args.command == 'train' and args.pathTrain is not None:
#        seqTrain = filter_seq(args.pathTrain, inSeqs)
#    else:
#        seqTrain = inSeqs#
#
 #   if args.pathVal is None:
#        random.shuffle(seqTrain)
#        sizeTrain = int(0.9 * len(seqTrain))
#        seqTrain, seqVal = seqTrain[:sizeTrain], seqTrain[sizeTrain:]
#    elif args.pathVal is not None:
#        seqVal = filter_seq(args.pathVal, inSeqs)
#        print(len(seqVal), len(inSeqs), args.pathVal)#
#
#    if args.debug:
#        seqVal = seqVal[:100]
#
#    print(f"Loading the validation dataset at {args.pathDB}")
#    datasetVal = per_src.SingleSequenceDataset(args.pathDB, seqVal,
#                                               phoneLabels, inDim=args.in_dim)
#
#    valLoader = DataLoader(datasetVal, batch_size=args.batchSize,
#                           shuffle=True)
